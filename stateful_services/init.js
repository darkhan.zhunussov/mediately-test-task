db.createUser({
    user: 'root',
    pwd: 'root',
    roles: [
        {
            role: 'readWrite',
            db: 'mediately',
        },
    ],
});

db = new Mongo().getDB("mediately");

db.createCollection('mediately', { capped: false });