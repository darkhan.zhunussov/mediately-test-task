import requests


class AirportRepository:
    @staticmethod
    def get_all():
        url = 'https://viennaairport.com/passagiere/ankunft__abflug/abfluege'
        r = HTMLSession()
        from response import response
        soup = BeautifulSoup(response, 'html.parser')
        table = soup.find('div', {'id': 'flugdaten-abflug'})
        flights = table.find_all('div', {'class': 'detail-table__row'})
        flights_models = []
        for flight in flights[1:]:
            try:
                route = flight.find('div', {'class': 'details__header text-uppercase detail-con'})
                time_flight = flight.find('div', {'class': 'detail-table__cell'})
                source_city = route.text.split(' - ')[0]
                source_weather = requests.get(
                    url=f'api.openweathermap.org/data/2.5/weather?q={source_city}&appid=f15c8728172d2e5471fd8b9dd6b10d28'
                )
                destination_city = route.text.split(' - ')[1].split()[0]
                destination_weather = requests.get(
                    url=f'api.openweathermap.org/data/2.5/weather?q={destination_city}&appid=f15c8728172d2e5471fd8b9dd6b10d28'
                )
                time_flight = time_flight.text
                f = AirportModel(
                    source_city=source_city,
                    source_weather=source_weather,
                    destination_city=destination_city,
                    destination_weather=destination_weather,
                    time_flight=time_flight
                )
                flights_models.append(f)
            except:
                continue
        return flights_models