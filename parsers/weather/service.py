from typing import List

from parsers.base_service import BaseService
from parsers.weather.dto import WeatherModel
from parsers.weather.repository import WeatherRepository


class WeatherService(BaseService):
    repository = WeatherRepository

    def get_all(self) -> List[WeatherModel]:
        result: WeatherModel = WeatherModel(
            **await self.repository.get_all()
        )
        return result

    def get_or_insert_cache(self):
        pass

    def save_to_db(self):
        pass
