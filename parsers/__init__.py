from typing import Tuple

from parsers.airport.service import AirportService
from parsers.weather.service import WeatherService

__all__: Tuple = (
    "AirportService",
    "WeatherService",
)
