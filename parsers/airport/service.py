from typing import List

from parsers.airport.dto import AirportModel
from parsers.airport.repository import AirportRepository
from parsers.base_service import BaseService


class AirportService(BaseService):
    repository = AirportRepository

    def get_all(self) -> List[AirportModel]:
        result: AirportModel = AirportModel(
            **await self.repository.get_all()
        )
        return result

    def get_or_insert_cache(self):
        pass

    def save_to_db(self):
        pass
