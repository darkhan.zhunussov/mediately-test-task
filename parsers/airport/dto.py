from pydantic import BaseModel


class AirportModel(BaseModel):
    id: str
    source_city: str
    source_weather: str
    destination_city: str
    destination_weather: str
    time_flight: str
