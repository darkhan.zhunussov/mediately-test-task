from abc import ABCMeta
from typing import List

from pydantic import BaseModel


class BaseService(metaclass=ABCMeta):
    def get_all(self) -> List[BaseModel]:
        raise NotImplementedError()

    def get_or_insert_cache(self):
        raise NotImplementedError()

    def save_to_db(self):
        raise NotImplementedError()
