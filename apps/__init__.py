from typing import Tuple

from apps.flight.controller import FlightController

__all__: Tuple = (
    "FlightController",
)
